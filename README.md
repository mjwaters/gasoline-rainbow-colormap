# Gasoline Rainbow Colormap

gasoline_rainbow is a colormap with very high local contrast while maintaining perceptual uniformity and linear luminance. It was designed by arranging knot points in a spiral around a spheroid in CAM02-UCS which are then fit with [VISCM](https://github.com/matplotlib/viscm). It can be considered a moderization of Matplotlib's [*prism*](https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html#sphx-glr-tutorials-colors-colormaps-py) colormap. Here is the VISCM demonstration of gasoline_rainbow:

![VISCM](gasoline_rainbow.png "gasoline_rainbow")

For reference, the Matplotlib default, *viridis*, has a length of 123.9. This means that gasoline_rainbow has 5.3x the total perceivable levels (total contrast) of viridis!

To regenerate the .jscm file, run the ```generate_jscm_without_hex_colors.py``` script:

```
python generate_jscm_without_hex_colors.py
```

This will generate the ```gasoline_rainbow.jscm``` with blank hexadecimal colors since we need VISCM to interpolate the knots. To complete the ```gasoline_rainbow.jscm``` file, run:

```
python -m viscm edit gasoline_rainbow.jscm
```

Then save and overwrite the exsiting ```gasoline_rainbow.jscm``` since the new one will have the interpolated hexadecimal colors. From VISCM, you can also save a new ```gasoline_rainbow.py``` file for use with Matplotlib or make another demonstration image like the one above.