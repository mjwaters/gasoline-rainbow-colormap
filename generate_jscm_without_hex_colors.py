import numpy as np

name = 'gasoline_rainbow'
filename = '%s.jscm'%name





# gr_4
cycles=  6
radius = 22
theta_0 = 50* np.pi/180
points_per_cycle =30 #20 #10
min_Jp = 26
max_Jp = 90


dtheta = 2*np.pi/points_per_cycle
thetas = -np.arange(0, 2*np.pi*cycles+dtheta/2.0, dtheta )+ theta_0

h = np.linspace(-1.0,1.0, len(thetas)) # scaled height

# creates spheriod in CAM02-UCS
radii =  radius* np.sqrt(1- h**2)


#------
#####
if False:
    class UCS_ball:
        def __init__( radius = 20, Jp_center = 50, cycles =  6, theta_0 = 50* np.pi/180, knots_per_cycle = 10):
            self.radius = radius
            self.Jp_center = Jp_center
            self.theta_0 = theta_0
            self.cycles    = cycles
            self.knots_per_cycle = points_per_cycle

        #def get_sRGB1_knots

########


head = '''{
    "content-type": "application/vnd.matplotlib.colormap-v1+json",
    "name": "%s",
    "license": "http://creativecommons.org/publicdomain/zero/1.0/",
    "usage-hints": [
        "red-green-colorblind-safe",
        "greyscale-safe",
        "sequential"
    ],
    "colorspace": "sRGB",
    "domain": "continuous",
    "colors": "",
    "extensions": {
        "https://matplotlib.org/viscm": {
            "min_Jp": %f,
            "max_Jp": %f,'''%(name,min_Jp,max_Jp)

point_string = ''
point_string = point_string + '\n            "xp": ['
for radius,theta in zip(radii,thetas):
    point_string = point_string +'\n                %f,'% (radius*np.cos(theta))
point_string = point_string[:-1]
point_string = point_string +'\n            ],'


point_string = point_string +'\n            "yp": ['
for radius,theta in zip(radii,thetas):
    point_string = point_string + '\n                %f,'% (radius*np.sin(theta))
point_string = point_string[:-1]
point_string = point_string +'\n            ],'


tail = '''
            "fixed": -1,
            "filter_k": 100,
            "cmtype": "linear",
            "uniform_colorspace": "CAM02-UCS",
            "spline_method": "CatmulClark"
        }
    }
}'''


fid = open(filename,'w')
fid.write(head)
fid.write(point_string)
fid.write(tail)
fid.close()
